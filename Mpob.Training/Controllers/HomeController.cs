using System.Web.Mvc;
using System.Linq;
using Mpob.Training.Models;


namespace Mpob.Training.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult Index ()
    {
      var m = Current.Db.Staff.All ().ToList();
      return View (m);
    }

    public ActionResult Edit (int id = 0)
    {
      var m = Current.Db.Staff.Get (id);
      return View (m);
    }

    public ActionResult Delete (int id = 0)
    {
      Current.Db.Staff.Delete (id);
      return RedirectToAction("Index");
    }

    public ActionResult About(int id = 1)
    {
      ViewBag.Id = id;
      var m = Current.Db.Query<Staff>("select * from staff where id=@id", new { id }).FirstOrDefault();
      return View (m);
    }
  }
}