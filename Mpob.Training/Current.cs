using System;
using Mpob.Training.Models;
using System.Web;

namespace Mpob.Training
{
  public class Current
  {
    public static Db Db {
      get {
        var db = HttpContext.Current.Items ["Db"] as Db;
        if (db != null) return Db;
        var cn = new MySql.Data.MySqlClient.MySqlConnection 
          ("server=localhost;user=root;password=tante;database=mpob_training;");
        db = Db.Init (cn, 30);
        HttpContext.Current.Items ["Db"] = db;
        return db;
      }
    }

    public static void DisposeDb()
    {
      var db = HttpContext.Current.Items ["Db"] as Db;
      if (db == null) return;
      db.Dispose ();
    }
  }
}

