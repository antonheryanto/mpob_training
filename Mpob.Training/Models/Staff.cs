using System;
using System.Collections.Generic;


namespace Mpob.Training.Models
{
  public class Staff
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public bool IsActive { get; set; }
    public DateTime? Joined { get; set; }
    public DateTime? Created { get; set; }
    public DateTime Changed { get; set; }

    public string Title { get; set; }
    public List<string> Phones { get; set; }
  }
}